# Take in branch name as a sysarg.
# like
# shell.sh develop
# if no branch name is specified then it will default to ALL_BRANCHES
# like
# shell.sh
BRANCH_NAME=${1:-ALL_BRANCHES}

rm *.pkl
wait
python3 call_api.py 1 1000 $BRANCH_NAME &
python3 call_api.py 1000 2000 $BRANCH_NAME &
python3 call_api.py 2000 3000 $BRANCH_NAME &
python3 call_api.py 3000 4000 $BRANCH_NAME &
python3 call_api.py 4000 5000 $BRANCH_NAME &
python3 call_api.py 5000 6000 $BRANCH_NAME &
wait

python3 call_api.py 6000 7000 $BRANCH_NAME &
python3 call_api.py 7000 8000 $BRANCH_NAME &
python3 call_api.py 8000 9000 $BRANCH_NAME &
python3 call_api.py 9000 10000 $BRANCH_NAME &
python3 call_api.py 10000 11000 $BRANCH_NAME &
python3 call_api.py 11000 12000 $BRANCH_NAME &
wait

python3 make_job_graphs.py $BRANCH_NAME
