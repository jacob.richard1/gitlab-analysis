import json
import urllib3
import csv
import sys
import pickle
import time
from concurrent.futures import ThreadPoolExecutor
http = urllib3.PoolManager()
metrics_dictionary = {}
start_page, end_page, branch_name = int(sys.argv[1]), int(sys.argv[2]), str(sys.argv[3])
def get_api_output(url, headers):
    r = http.request('GET', url, headers=headers)
    decoded = r.data.decode('utf-8')
    #exponential backoff
    wait = 1
    while wait < 2000 and '500 Internal Server Error' in decoded:
        wait = wait*2
        r = http.request('GET', url, headers=headers)
        decoded = r.data.decode('utf-8')
        time.sleep(.00001*wait)
    if wait >= 2000:
        return None
    if wait > 1:
        print(f'retry success for url {url}')
    return decoded
def return_metrics(page: int):
    metrics_d = {}
    fields = []
    url=f"https://gitlab.com/api/v4/projects/12706411/jobs?per_page=50&page={page}"
    headers = {'PRIVATE-TOKEN': token}
    if (page % 200) == 0:
        print(page)
    decoded = get_api_output(url, headers)
    try:
        pipelines = json.loads(decoded)
    except Exception as e:
        print(f"API request failed for page {page} and message {decoded}")
        return [], {}
    for pipeline in json.loads(decoded):
        try:
            month = pipeline['created_at'][0:7]
        except:
            print(f'pipeline failed for page number: {page}, text: {decoded}')
            exit()
        status, name, branch_name_pipeline = pipeline['status'], pipeline['name'], pipeline['ref']
        duration = pipeline['duration'] if pipeline['duration'] else 0
        if status in ['failed', 'success'] and (branch_name=='ALL_BRANCHES' or branch_name_pipeline == branch_name):
            if name not in metrics_d:
                metrics_d[name] = {}
            if month not in metrics_d[name]:
                metrics_d[name][month] = {'status':[], 'duration':[]}
            metrics_d[name][month]['status'].append(status)
            metrics_d[name][month]['duration'].append(duration)
            fields.append(month)
        else:
            time.sleep(.00001)
    fields = list(set(fields))
    output_dictionary = {}
    for name in metrics_d:
        for month in metrics_d[name]:
            for n in [f'total_jobs_{name}', f'percent_failed_{name}', f'average_duration_{name}']:
                output_dictionary[n] = {}
            status_out = metrics_d[name][month]['status']
            duration_out = metrics_d[name][month]['duration']
            output_dictionary[f'total_jobs_{name}'][month] = len(status_out)
            output_dictionary[f'percent_failed_{name}'][month] = status_out.count('failed')
            output_dictionary[f'average_duration_{name}'][month] = sum(duration_out)
    return [fields, output_dictionary]
pages = [page for page in range(start_page,end_page)]
with ThreadPoolExecutor(max_workers=20) as executor:
    future_results = executor.map(return_metrics, pages)
    results = [result for result in future_results]
with open(f'out-{start_page}-{end_page}.pkl', 'wb') as f:
    pickle.dump(results, f)
