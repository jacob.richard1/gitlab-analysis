import json
import urllib3
import csv
import sys
import pickle
import os
http = urllib3.PoolManager()
metrics_dictionary = {}
fields, results = [], []
branch_name = str(sys.argv[1])
for file in os.listdir('./'):
    if file.endswith('.pkl'):
        with open(file, 'rb') as f:
            results+= pickle.load(f)
for fiel, metrics_d in results:
    fields+=fiel
    for name in metrics_d:
        for month in metrics_d[name]:
            if name not in metrics_dictionary:
                metrics_dictionary[name] = {}
            if month not in metrics_dictionary[name]:
                metrics_dictionary[name][month] = 0
            metrics_dictionary[name][month]+=metrics_d[name][month]
for name in metrics_dictionary:
    for month in metrics_dictionary[name]:
        temp_name = '_'.join(name.split('_')[2:])
        if name.startswith('percent_failed_'):
            metrics_dictionary[name][month]/=metrics_dictionary[f'total_jobs_{temp_name}'][month]
            metrics_dictionary[name][month]=round(metrics_dictionary[name][month]*100, 2)
        if name.startswith('average_duration_'):
            metrics_dictionary[name][month]/=metrics_dictionary[f'total_jobs_{temp_name}'][month]
            metrics_dictionary[name][month]=round(metrics_dictionary[name][month], 2)
fields = ['name'] + sorted(list(set(fields)))
with open(f"{branch_name}-out.csv", "w") as f:
    w = csv.DictWriter(f, fields )
    w.writeheader()
    for key,val in sorted(metrics_dictionary.items()):
        row = {'name': key}
        row.update(val)
        w.writerow(row)
